# reverse-proxy-packer

## Packer build file
The packer build file used for this is located [here](https://gitlab.com/ucl-itt-ss3/reverse-proxy/blob/master/reverse_proxy.json)

## Packer Guides for VMWare
* From existing VMs [link](https://www.packer.io/docs/builders/vmware-vmx.html)
* From ISO files [link](https://www.packer.io/docs/builders/vmware-iso.html)